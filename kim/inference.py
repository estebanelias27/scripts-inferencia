#librerias 

import torch 
import torch.nn as nn
import torch.nn.functional as F
import librosa
from vggish_utils import vggish_input_clipwise
import numpy as np
import oyaml as yaml
import csv


#cargar las rutas de todos los archivos
import os
from os import listdir
from os.path import isfile, join
audios_lista = [f for f in listdir('./audios') if isfile(join('./audios', f))]



#mel tomado de extract_mel.py

def wav_to_mel(filename):
	y, sr = librosa.load(filename, mono=True, sr=None)
	if y.shape[0]<sr*1 and y.shape[0]>sr*0.0:
		y=librosa.util.fix_length(y, int(sr*1.01))
	y = y.T
	
	mel = vggish_input_clipwise.waveform_to_examples(y, sr)

	return mel

#generando lista de mel
logmel_list = []
for audio_ruta in audios_lista:
    logmel_list.append(wav_to_mel('./audios/'+audio_ruta))


#modelo importado de model_architecture.py
class MyCNN(nn.Module):
	def __init__(self):
		super(MyCNN, self).__init__()

		self.layer1_conv1 = nn.Sequential(nn.Conv2d(1, 64, kernel_size=3, stride=1, padding=1), nn.ReLU())
		self.layer2_pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

									
		self.layer3_conv2 = nn.Sequential(nn.Conv2d(64, 128,kernel_size=3, stride=1, padding=1), nn.ReLU())
		self.layer4_pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

		self.layer5_conv3_1 = nn.Sequential(nn.Conv2d(128, 256,kernel_size=3, stride=1,padding=1), nn.ReLU())
		self.layer6_conv3_2 = nn.Sequential(nn.Conv2d(256, 256,kernel_size=3, stride=1,padding=1), nn.ReLU())
		self.layer7_pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

		self.layer8_conv4_1 = nn.Sequential(nn.Conv2d(256, 512,kernel_size=3, stride=1,padding=1), nn.ReLU())
		self.layer9_conv4_2 = nn.Sequential(nn.Conv2d(512, 512,kernel_size=3, stride=1,padding=1), nn.ReLU())
		
		self.new_fc1 = nn.Sequential(nn.Linear(4096, 2048), nn.ReLU())
		self.new_fc2 = nn.Sequential(nn.Linear(2048, 128), nn.ReLU())

		self.final= nn.Sequential(nn.Linear(128, 8), nn.Sigmoid())

	
	def forward(self, x):

		out = self.layer1_conv1(x)
		out = self.layer2_pool1(out)

		out = self.layer3_conv2(out)
		out = self.layer4_pool2(out)

		out = self.layer5_conv3_1(out)
		out = self.layer6_conv3_2(out)
		out = self.layer7_pool3(out)

		out = self.layer8_conv4_1(out)
		out = self.layer9_conv4_2(out)

		# maxpooling
		out = torch.max(out, dim=2)[0]
		out = out.view(out.size(0),-1)
	
		out = self.new_fc1(out)
		out = self.new_fc2(out)
		out = self.final(out)
	
		return out

#predecir tomado de train.py

# def predict(mel_list, test_file_idxs, model):
# 	"""
# 	Modified predict_framewise() in classify.py of the baseline code

# 	"""
# 	y_pred = []

# 	with torch.no_grad():
# 		for idx in test_file_idxs:

# 			test_x = mel_list[idx]
# 			test_x = np.reshape(test_x,(1,1,test_x.shape[0],test_x.shape[1]))

# 			if torch.cuda.is_available():
# 				test_x = torch.from_numpy(test_x).cuda().float()
# 			else:
# 				test_x = torch.from_numpy(test_x).float()
			
# 			model_output = model(test_x)

# 			if torch.cuda.is_available():
# 				model_output = model_output.cpu().numpy()[0].tolist()
# 			else:
# 				model_output = model_output.numpy()[0].tolist()

# 			y_pred.append(model_output)

# 	return y_pred

def predict(mel_list, test_file_idxs, model):
	"""
	Modified predict_framewise() in classify.py of the baseline code

	"""
	y_pred = []

	with torch.no_grad():
		for idx in test_file_idxs:

			test_x = mel_list[idx]
			test_x = np.reshape(test_x,(1,1,test_x.shape[0],test_x.shape[1]))

			if torch.cuda.is_available():
				test_x = torch.from_numpy(test_x).cuda().float()
			else:
				test_x = torch.from_numpy(test_x).float()
			
			model_output = model(test_x)

			if torch.cuda.is_available():
				model_output = model_output.cpu().numpy()[0].tolist()
			else:
				model_output = model_output.numpy()[0].tolist()

			y_pred.append(model_output)

	return y_pred

########################################################################


#test_file_idxs = range(len(file_list))

test_file_idxs = range(len(logmel_list))

#tomado de gen_sunmission.py


#############PREDICTION####################################################
model = MyCNN()

models_dir = 'checkpoints'

model_list = [f for f in os.listdir(models_dir) if 'pth' in f]

val_loss = [float(f.split('_')[-1][:-4]) for f in model_list]
model_filename = model_list[np.argmin(val_loss)]

model.load_state_dict(torch.load(os.path.join(models_dir, model_filename)))

if torch.cuda.is_available():
    model.cuda()
model.eval()


y_pred = predict(logmel_list, test_file_idxs, model)
###########################################################################


print(len(y_pred))


#tomado de classify.py (baseline code)

def generate_output_file(y_pred, test_file_idxs, results_dir, file_list,
                         aggregation_type, label_mode, taxonomy):
    """
    Write the output file containing model predictions

    Parameters
    ----------
    y_pred
    test_file_idxs
    results_dir
    file_list
    aggregation_type
    label_mode
    taxonomy

    Returns
    -------

    """
    output_path = os.path.join(results_dir, "output_{}.csv".format(aggregation_type))
    test_file_list = [file_list[idx] for idx in test_file_idxs]

    coarse_fine_labels = [["{}-{}_{}".format(coarse_id, fine_id, fine_label)
                             for fine_id, fine_label in fine_dict.items()]
                           for coarse_id, fine_dict in taxonomy['fine'].items()]

    full_fine_target_labels = [fine_label for fine_list in coarse_fine_labels
                                          for fine_label in fine_list]
    coarse_target_labels = ["_".join([str(k), v])
                            for k,v in taxonomy['coarse'].items()]


    with open(output_path, 'w') as f:
        csvwriter = csv.writer(f)

        # Write fields
        fields = ["audio_filename"] + full_fine_target_labels + coarse_target_labels
        csvwriter.writerow(fields)

        # Write results for each file to CSV
        for filename, y, in zip(test_file_list, y_pred):
            row = [filename]

            if label_mode == "fine":
                fine_values = []
                coarse_values = [0 for _ in range(len(coarse_target_labels))]
                coarse_idx = 0
                fine_idx = 0
                for coarse_label, fine_label_list in zip(coarse_target_labels,
                                                         coarse_fine_labels):
                    for fine_label in fine_label_list:
                        if 'X' in fine_label.split('_')[0].split('-')[1]:
                            # Put a 0 for other, since the baseline doesn't
                            # account for it
                            fine_values.append(0.0)
                            continue

                        # Append the next fine prediction
                        fine_values.append(y[fine_idx])

                        # Add coarse level labels corresponding to fine level
                        # predictions. Obtain by taking the maximum from the
                        # fine level labels
                        coarse_values[coarse_idx] = max(coarse_values[coarse_idx],
                                                        y[fine_idx])
                        fine_idx += 1
                    coarse_idx += 1

                row += fine_values + coarse_values

            else:
                # Add placeholder values for fine level
                row += [0.0 for _ in range(len(full_fine_target_labels))]
                # Add coarse level labels
                row += list(y)

            csvwriter.writerow(row)

###########################################################################

aggregation_type = 'max'
label_mode='coarse'

output_dir='system_1'

with open('data/dcase-ust-taxonomy.yaml', 'r') as f:
    taxonomy = yaml.load(f, Loader=yaml.Loader)


os.makedirs(output_dir, exist_ok=True)
generate_output_file(y_pred, test_file_idxs, output_dir, audios_lista,
                                aggregation_type, label_mode, taxonomy)