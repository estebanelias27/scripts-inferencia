#argumentos libreria
import argparse

#Cargando librerias

import torch
import numpy as np
import torch.nn as nn
import torchvision.models
import librosa
from torch.utils.data import Dataset, DataLoader
import pandas as pd

#argumentos
parser = argparse.ArgumentParser(description='Realiza inferencias utilizando adapa_task5')


#modelo de adapa_task 5 (Obtenido de utils)

class Task5Model(nn.Module):

    def __init__(self, num_classes):

        super().__init__()

        self.bw2col = nn.Sequential(
            nn.BatchNorm2d(1),
            nn.Conv2d(1, 10, 1, padding=0), nn.ReLU(),
            nn.Conv2d(10, 3, 1, padding=0), nn.ReLU())

        self.mv2 = torchvision.models.mobilenet_v2(pretrained=True)

        self.final = nn.Sequential(
            nn.Linear(1280, 512), nn.ReLU(), nn.BatchNorm1d(512),
            nn.Linear(512, num_classes))

    def forward(self, x):
        x = self.bw2col(x)
        x = self.mv2.features(x)
        x = x.max(dim=-1)[0].max(dim=-1)[0]
        x = self.final(x)
        return x
##############################################


#clase dataset de 10-generate-submission-system-1

class AudioDataset(Dataset):
    def __init__(self, X):
        self.X = X

    def __len__(self):
        return self.X.shape[0]

    def __getitem__(self, idx):
        sample = self.X[idx, ...]
        i = np.random.randint(sample.shape[1])
        sample = torch.cat([
                sample[:, i:, :],
                sample[:, :i, :]],
                dim=1)
        return sample

################################################

#cargar las rutas de todos los archivos

from os import listdir
from os.path import isfile, join
audios_lista = [f for f in listdir('./audios') if isfile(join('./audios', f))]


#preparación de datos log -MEl Spectogram (Obtenido de 02-compute-log-mel)

logmel_list = []

def compute_melspec(filename):
    wav = librosa.load(filename, sr=44100)[0]
    melspec = librosa.feature.melspectrogram(
        wav,
        sr=44100,
        n_fft=128*20,
        hop_length=347*2,
        n_mels=128,
        fmin=20,
        fmax=44100 // 2)
    logmel = librosa.core.power_to_db(melspec)
    #np.save(outdir + os.path.basename(filename) + '.npy', logmel)
    return logmel

for audio_ruta in audios_lista:
    logmel_list.append(compute_melspec('./audios/'+audio_ruta))

##############################################################################

#tomado de 10-generate-submission-system-1
# X = np.concatenate([
#         np.expand_dims(np.load('./data/logmelspec-eval/{}.npy'.format(x)).T[:635, :], axis=0)
#         for x in eval_files])
# X = X[:, None, :, :]
X = np.concatenate([
        np.expand_dims(audio.T[:635, :], axis=0)
        for audio in logmel_list])
X = X[:, None, :, :]
##########################################

dataset = AudioDataset(torch.Tensor(X))
loader = DataLoader(dataset, 64, shuffle=False)



cuda = True
device = torch.device('cuda:0' if cuda else 'cpu')
print('Device: ', device)

model = Task5Model(31).to(device)
model.load_state_dict(torch.load('./model_system1'))


all_preds = []
for _ in range(10):
    preds = []
    for inputs in loader:
            inputs = inputs.to(device)
            with torch.set_grad_enabled(False):
                model = model.eval()
                outputs = model(inputs)
                preds.append(outputs.detach().cpu().numpy())
    preds = np.concatenate(preds, axis=0)
    preds = (1 / (1 + np.exp(-preds)))
    all_preds.append(preds)
tmp = all_preds[0]
for x in all_preds[1:]:
    tmp += x
tmp = tmp / 10
preds = tmp

output_df = pd.DataFrame(
    preds, columns=[
        '1_engine', '2_machinery-impact', '3_non-machinery-impact',
        '4_powered-saw', '5_alert-signal', '6_music', '7_human-voice', '8_dog',
        '1-1_small-sounding-engine', '1-2_medium-sounding-engine',
        '1-3_large-sounding-engine', '2-1_rock-drill', '2-2_jackhammer',
        '2-3_hoe-ram', '2-4_pile-driver', '3-1_non-machinery-impact',
        '4-1_chainsaw', '4-2_small-medium-rotating-saw',
        '4-3_large-rotating-saw', '5-1_car-horn', '5-2_car-alarm', '5-3_siren',
        '5-4_reverse-beeper', '6-1_stationary-music', '6-2_mobile-music',
        '6-3_ice-cream-truck', '7-1_person-or-small-group-talking',
        '7-2_person-or-small-group-shouting', '7-3_large-crowd',
        '7-4_amplified-speech', '8-1_dog-barking-whining'])
output_df['audio_filename'] = pd.Series(audios_lista, index=output_df.index)

for x in [
        '1-X_engine-of-uncertain-size', '2-X_other-unknown-impact-machinery',
        '4-X_other-unknown-powered-saw', '5-X_other-unknown-alert-signal',
        '6-X_music-from-uncertain-source', '7-X_other-unknown-human-voice']:
    output_df[x] = 0

cols_in_order = [
    "audio_filename", "1-1_small-sounding-engine",
    "1-2_medium-sounding-engine", "1-3_large-sounding-engine",
    "1-X_engine-of-uncertain-size", "2-1_rock-drill",
    "2-2_jackhammer", "2-3_hoe-ram", "2-4_pile-driver",
    "2-X_other-unknown-impact-machinery", "3-1_non-machinery-impact",
    "4-1_chainsaw", "4-2_small-medium-rotating-saw",
    "4-3_large-rotating-saw", "4-X_other-unknown-powered-saw",
    "5-1_car-horn", "5-2_car-alarm", "5-3_siren", "5-4_reverse-beeper",
    "5-X_other-unknown-alert-signal", "6-1_stationary-music",
    "6-2_mobile-music", "6-3_ice-cream-truck",
    "6-X_music-from-uncertain-source", "7-1_person-or-small-group-talking",
    "7-2_person-or-small-group-shouting", "7-3_large-crowd",
    "7-4_amplified-speech", "7-X_other-unknown-human-voice",
    "8-1_dog-barking-whining", "1_engine", "2_machinery-impact",
    "3_non-machinery-impact", "4_powered-saw", "5_alert-signal",
    "6_music", "7_human-voice", "8_dog"]
output_df = output_df.loc[:, cols_in_order]

output_df.to_csv('submission-system-1.csv', index=False)
